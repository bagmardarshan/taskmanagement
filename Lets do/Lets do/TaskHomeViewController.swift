//
//  TaskHomeViewController.swift
//  Lets do
//
//  Created by Darshan Bagmar on 21/06/20.
//  Copyright © 2020 Darshan Bagmar. All rights reserved.
//

import UIKit
import PageMenu
import SideMenu

class TaskHomeViewController: UIViewController {
    var pageMenu : CAPSPageMenu?
    var controllerArray : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myTaskVC : UIViewController = UIViewController(nibName: "MyTaskViewController", bundle: nil)
        myTaskVC.title = "MY TASKS"
        controllerArray.append(myTaskVC)
        
        let assignedTaskVC : UIViewController = UIViewController(nibName: "AssignedTaskViewController", bundle: nil)
        assignedTaskVC.title = "ASSIGNED TASKS"
        controllerArray.append(assignedTaskVC)
        
        let allTaskVC : UIViewController = UIViewController(nibName: "AllTasksViewController", bundle: nil)
        allTaskVC.title = "ALL TASKS"
        controllerArray.append(allTaskVC)

        
         let parameters: [CAPSPageMenuOption] = [
                   .scrollMenuBackgroundColor(UIColor.orange),
                   .viewBackgroundColor(UIColor.white),
                   .selectionIndicatorColor(UIColor.white),
                   .unselectedMenuItemLabelColor(UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.4)),
                   .menuItemFont(UIFont(name: "HelveticaNeue", size: 15.0)!),
                   .menuHeight(44.0),
                   .menuMargin(20.0),
                   .selectionIndicatorHeight(4.0),
                   .bottomMenuHairlineColor(UIColor.orange),
                   .menuItemWidthBasedOnTitleTextWidth(true),
                   .selectedMenuItemLabelColor(UIColor.white)
               ]


        pageMenu?.delegate = self as? CAPSPageMenuDelegate
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 90.0, width: self.view.frame.width, height: self.view.frame.height - 60.0), pageMenuOptions: parameters)
               

        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        self.navigationController?.navigationBar.isHidden = true
        self.view.addSubview(pageMenu!.view)
        // Do any additional setup after loading the view.
    }
    //MARK:- onClickMenu
    @IBAction func onClickMenuButton(_ sender: Any) {
        let menuVC:UIViewController = UIViewController(nibName: "MenuViewController", bundle: nil)
    
        let menu = SideMenuNavigationController(rootViewController: menuVC)
        menu.leftSide = true
        present(menu, animated: true, completion: nil)
        
        
    }
    
    //MARK:- onClickCreateTask
    @IBAction func onClickCreateTask(_ sender: Any) {
        let createTaskVC:UIViewController = UIViewController(nibName: "CreateTaskViewController", bundle: nil)
        self.navigationController?.pushViewController(createTaskVC, animated: true)
    }
    
     //MARK:- onClickNotificationButton
    @IBAction func onClickNotificationButton(_ sender: Any) {
        let notificationVC:UIViewController = UIViewController(nibName: "NotificationViewController", bundle: nil)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension TaskHomeViewController:SideMenuNavigationControllerDelegate{
    func sideMenuWillAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appearing! (animated: \(animated))")
    }
    
    func sideMenuDidAppear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Appeared! (animated: \(animated))")
    }
    
    func sideMenuWillDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappearing! (animated: \(animated))")
    }
    
    func sideMenuDidDisappear(menu: SideMenuNavigationController, animated: Bool) {
        print("SideMenu Disappeared! (animated: \(animated))")
    }
}
